<?php
class Processor_OxShopControl extends Processor_OxShopControl_parent
{
    /**
     * render oxView object
     *
     * @param oxView $oViewObject view object to render
     *
     * @return string
     */
    protected function _render($oViewObject)
    {
        $sOutput = parent::_render($oViewObject);

        // Remove silly OXID version information
        // There is a small chance that this is slightly illegal.
        // $pattern = "/\ *<!-- OXID eShop Community Edition,.* --\>/";
        // $sOutput = preg_replace($pattern, '', $sOutput);

        /*
        // Tidy Indenting
        $lines = explode("\n", $sOutput);
        $sOutput = '';
        foreach ($lines as $line) {
            $line = trim($line);
            if ($line != "") {
                $sOutput .= $line."\n";
            }
        }

        // Basic indenting
        $sOutput = preg_replace("/\<div/",   "    <div" , $sOutput);
        $sOutput = preg_replace("/\<\/div/", "    </div", $sOutput);

        $sOutput = preg_replace("/\<p/",   "        <p", $sOutput);
        $sOutput = preg_replace("/\<\/p/", "        </p", $sOutput);

        $sOutput = preg_replace("/\<ul/",   "        <ul", $sOutput);
        $sOutput = preg_replace("/\<\/ul/", "        </ul", $sOutput);

        $sOutput = preg_replace("/\<li/",   "            <li", $sOutput);
        $sOutput = preg_replace("/\<\/li/", "            </li", $sOutput);

        // Unnecessary spaces
        $sOutput = preg_replace("/\> *\</", "><", $sOutput);
        */

        // Fix annoyingly spaced hrefs
        $sOutput = preg_replace("/ *href=/",   " href=" , $sOutput);

        // Fix badly spaced attributes
        // $sOutput = preg_replace("/\ *\"\>/", "\">", $sOutput);

        // Fix incorrectly spaced LIs
        // $sOutput = str_replace('<li >', '<li>', $sOutput);

        // Fix empty class tags
        $sOutput = preg_replace("/class=\"\"/", "", $sOutput);

        // Tidy <head>.*</head>
        $pattern = "/\<head\>(?<head>.*)\<\/head\>/s";
        $sOutput = preg_replace_callback($pattern, array($this, '_processor_callback_tidyhead'), $sOutput);

        return $sOutput;
    }


    protected function _processor_callback_tidyhead($match)
    {
        $head = '';
        $originalHead = explode("\n", $match['head']);
        foreach ($originalHead as $line) {
            $line = trim($line);
            if ($line != "") {
                $head .= '    '.trim($line)."\n";
            }
        }

        return "<head>\n".$head."</head>";
    }

}
