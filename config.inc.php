<?php
/**
 * Processor - LESS Parser & CSS/JS minifier
 *
 * @version 1.0.0
 * @author Dave Holloway <dh@gn2-netwerk.de>
 * @author (contributing) jdlx <honeypot@rexdev.de>
 */

require_once 'Processor.php';
require_once $REX['INCLUDE_PATH'].'/addons/processor/lessc.php';
require_once $REX['INCLUDE_PATH'].'/addons/processor/cssmin.php';
require_once $REX['INCLUDE_PATH'].'/addons/processor/jsmin.php';
require_once $REX['INCLUDE_PATH'].'/addons/processor/urirewriter.php';
