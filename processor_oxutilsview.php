<?php
class Processor_OxUtilsView extends Processor_OxUtilsView_parent
{
    public function getSmarty($blReload = false)
    {
        if (!class_exists('Processor')) {
            $p = dirname(__FILE__);
            require_once $p . '/Processor.php';
            require_once $p.'/lessc.php';
            require_once $p.'/cssmin.php';
            require_once $p.'/jsmin.php';
        }

        $oConfig = oxRegistry::getConfig();
        $path = rtrim($oConfig->getConfigParam('sShopDir'), '/');
        $smarty = parent::getSmarty();
        $smarty->unregister_function('oxstyle');
        $smarty->register_function('oxstyle', array($this,'processor_smarty_function_oxstyle'));
        $smarty->unregister_function('oxscript');
        $smarty->register_function('oxscript', array($this,'processor_smarty_function_oxscript'));

        foreach (array('smarty_function_oxstyle', 'smarty_function_oxscript') as $func) {
            if (!function_exists($func)) {
                $file = $path.'/core/smarty/plugins/'.str_replace('smarty_function_', 'function.', $func).'.php';
                require_once $file;
            }
        }
        return $smarty;
    }

    function processor_smarty_function_oxstyle($params, &$smarty)
    {
        $output = smarty_function_oxstyle($params, $smarty);
        return $this->processor_parse($output);
    }

    function processor_smarty_function_oxscript($params, &$smarty)
    {
        $output = smarty_function_oxscript($params, $smarty);
        return $this->processor_parse($output);
    }

    function processor_parse($output) {
        if (strlen($output) > 0) {
            $oConfig = oxRegistry::getConfig();
            $noSSL = str_replace('/', '\/', $oConfig->getConfigParam('sShopURL'));
            $SSL = str_replace('/', '\/', $oConfig->getConfigParam('sSSLShopURL'));
            $pattern = "/(?P<attr>href|src)=\"(?P<domain>".$noSSL."|".$SSL.")(?P<path>.*?)?(\?(.*))\"/";
            $output = preg_replace_callback($pattern, array($this, 'processor_replace_callback'), $output);
        }
        return $output;
    }

    function processor_replace_callback($m)
    {
        if (strpos($m['path'], '.min.') === false
            && strpos($m['path'], '.pack.') === false 
            && strpos($m['path'], 'out/admin/') === false) {
            $path = Processor::parse($m['path']);
        } else {
            $path = $m['path'];
        }
        $url = $m['attr'].'="'.$m['domain'].$path.'"';
        return $url;
    }
}
