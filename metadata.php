<?php
$sMetadataVersion = '1.1';



$aModule = array(
    'id'           => 'processor',
    'title'        => 'gn2 :: Processor',
    'description'  => '',
    'thumbnail'    => '',
    'version'      => '1.0',
    'author'       => 'gn2 netwerk',
    'extend'       => array(
        'oxutilsview'   => 'gn2netwerk/processor/processor_oxutilsview',
        'oxshopcontrol' => 'gn2netwerk/processor/processor_oxshopcontrol'
    )
);