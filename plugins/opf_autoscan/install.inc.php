<?php
/**
 * opf_autoscan - Processor Addon Plugin
 *
 * @version 0.1.0
 * @package redaxo 4.5.x
 */

$minimum_REX = '4.5.0';

if(version_compare($REX['VERSION'].'.'.$REX['SUBVERSION'].'.'.$REX['MINORVERSION'], $minimum_REX, '<'))
{
    $REX['ADDON']['installmsg']['opf_autoscan'] =  'Dieses Plugin ben&ouml;tigt Redaxo Version '.$minimum_REX.' oder h&ouml;her.';
    $REX['ADDON']['install']['opf_autoscan']    = 0;
    return;
}

$REX['ADDON']['install']['opf_autoscan'] = 1;
