<?php
/**
 * opf_autoscan - Processor Addon Plugin
 *
 * @version 0.1.0
 * @package redaxo 4.5.x
 */

if($REX['REDAXO']) {
    return;
}

rex_register_extension('OUTPUT_FILTER', 'opf_autoscan_matcher', array(), REX_EXTENSION_LATE);

function opf_autoscan_matcher($params)
{
    $regex = '@(<(?:link|script)[^>]*(?:href|src)=")((https|http)*[^"]+\.(css|less|js))("[^>]*>(?:</script>)*)@';
    return preg_replace_callback($regex, 'opf_autoscan_replacer', $params['subject']);
}

function opf_autoscan_replacer($match)
{

    // SKIP URLS WITH LEADING HTTP
    if($match[3] == 'http') {
        return $match[0];
    }

    static $processor = false;
    static $dev       = false;
    static $docroot   = false;
    global $REX;

    if(!$processor)
    {
        $processor = new Processor;

        $loggedIn = isset($_SESSION[$REX['INSTNAME']]['UID']) && $_SESSION[$REX['INSTNAME']]['UID'] > 0;
        if ($loggedIn && (!isset($REX['LOGIN']) || !is_object($REX['LOGIN'])))
        {
            if (!is_object($I18N))
            {
                $I18N = rex_create_lang($REX['LANG']);
            }
            $REX['LOGIN'] = new rex_backend_login($REX['TABLE_PREFIX'] . 'user');
            $loggedIn = $REX['LOGIN']->checkLogin();
        }

        $dev = $loggedIn && $REX['LOGIN']->USER->isAdmin() ? true : false;
        $docroot = rex_register_extension_point('PROCESSOR_AUTOSCAN_DOCROOT', null);
    }

    $sourcepath = ltrim($match[2],'/');

    return $match[1]. $processor->parse( $sourcepath, $dev, $docroot ) .$match[5];
}

